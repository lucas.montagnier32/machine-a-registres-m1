using Base

"""
Le nombre de registres disponibles.
"""
const NR = 8

#################
# Décompilation #
#################

"""
	filetomatrix(file)

Récupère un programme à partir d'un fichier `file` et le met dans une matrice d'entiers.
"""
function filetomatrix(file::String)::Array{Int64,2}
	stream = open(file,"r")
	sep = [' ']
	result = Array{Int64,2}(undef,countlines(file),3)
	counter = 1
	while  !eof(stream)
		ligne = readline(stream)
		pgm = split(ligne,sep)
		for i in 1:3
			result[counter,i] = parse(Int64,pgm[i])
		end
		counter += 1
	end
	close(stream)
	return result
end

"""
	IHM(pgm)

Prend en entrée un programme `pgm` et renvoie la version human-readable dans un tableau de chaînes de caractères.
"""
function IHM(pgm::Array{Int64,2})
	HRpgm::Array{String,1} = []
	for i in 1:size(pgm, 1)
		if(pgm[i,1]==1)
			push!(HRpgm, "R$(pgm[i,2]) <- $(pgm[i,3])")
		elseif(pgm[i,1]==2)
			push!(HRpgm, "R$(pgm[i,2]) <- R$(pgm[i,3])")
		elseif(pgm[i,1]==3)
			push!(HRpgm, "R$(pgm[i,2]) <- R$(pgm[i,2]) + R$(pgm[i,3])")
		elseif(pgm[i,1]==4)
			push!(HRpgm, "R$(pgm[i,2]) <- R$(pgm[i,2]) - R$(pgm[i,3])")
		elseif(pgm[i,1]==5)
			push!(HRpgm, "R$(pgm[i,2])=0 -> $(pgm[i,3])")
		end
	end
	return HRpgm
end

"""
	arraytofile(HRpgm)

Écrit dans un fichier dénommé `"HRpgm"` le tableau de chaine de caractère `HRpgm` du programme human-readable.
"""
function arraytofile(HRpgm::Array{String,1})
	touch("HRpgm")
	stream = open("HRpgm","w")
	for i in 1:length(HRpgm)
		println(HRpgm[i])
		write(stream, HRpgm[i], "\n")
	end
	close(stream)
end

"""
	decompile(file)

Transforme un programme dans un fichier dénommé `file` en une version human-readable dans un autre fichier dénommé `"HRpgm"`.
"""
function decompile(file::String)
	arraytofile(IHM(filetomatrix(file)))
end

###########
# Analyse #
###########

"""
	StackNode{T}

Un noeud de pile d'éléments de type `T`.
"""
struct StackNode{T}
	"""
	La valeur conservée par le noeud de pile.
	"""
	head::T
	"""
	Le précédent noeud empilé sur la pile.
	"""
	next::Union{StackNode{T}, Nothing}
end

"""
	Stack{T}

Une pile d'éléments de type `T`.
"""
const Stack{T} = Union{StackNode{T}, Nothing}

"""
	AnalysisExecution

Un fil d'exécution utilisé lors d'une analyse des registres initialisés d'un programme,
dans le but d'être suspendu et repris à un instant arbitraire.
"""
mutable struct AnalysisExecution
	"""
	L'indice de la prochaine instruction à exécuter.
	"""
	PC::Int64
	"""
	L'ensemble des registres pas encore initialisés.
	"""
	registers::BitVector
	"""
	La pile d'appel, des sauts réalisés.
	"""
	jumpstack::Stack{Int64}
end

function Base.print(io::IO, e::AnalysisExecution)
	print(io, e.PC)
	print(io, " - ")
	print(io, e.registers)
	print(io, " - ")
	print(io, e.jumpstack)
end

"""
	printwarn(PC, xs...)

Affiche un warning dans la sortie standard.
"""
function printwarn(PC::Int64, xs...)
	println("\033[1;94mWARNING[", PC, "]: ", xs..., "\033[0m")
end

"""
	printstack(stack)

Affiche une pile d'appel associée à un warnning dans la sortie standard.
"""
function printstack(stack::Stack{T}) where T
	while stack !== nothing
		println("\t\033[94m* when jumping from l. ", stack.head, "\033[0m")
		stack = stack.next
	end
end

"""
	printwarn(PC, xs...)

Affiche un warning `xs` dans la sortie standard, en rapport à une instruction d'indice `PC`.
"""
function printwarn(PC::Int64, xs...)
	println("\033[1;94mWARNING[", PC, "]:\033[0;94m ", xs..., "\033[0m")
end

"""
	printerr(PC, xs...)

Affiche un message d'erreur `xs` dans la sortie standard, en rapport à une instruction d'indice `PC`.
"""
function printerr(PC::Int64, xs...)
	println("\033[1;33mERROR[", PC, "]: ", xs..., "\033[0m")
end

"""
	chkregister(PC, register)

Indique si `register` est un numéro de registre valide, composant une instruction indicée `PC`.
"""
function chkregister(PC::Int64, register::Int64)::Bool
	if 1 > register || register > NR
		printerr(PC, "invalid register ", register)
		return false
	end

	return true
end

"""
	chklinenumber(PC, NI, linenumber)

Indique si `linenumber` est un indice d'instruction valide, composant une instruction indicée `PC` dans un programme à `NI` instructions.
"""
function chklinenumber(PC::Int64, NI::Int64, linenumber::Int64)::Bool
	ok = true

	if linenumber < 1
		printerr(PC, "invalid line number ", linenumber, ", negative numbers are not supported")
		ok = false
	elseif linenumber > NI + 1
		printwarn(PC, "invalid line number ", linenumber, ", the program only has ", NI, " lines")
	elseif linenumber ≡ PC
		printwarn(PC, "jumping to itself")
	end

	return ok
end

"""
	chkinstruction(PC, NI, i, x, y)

Indique si `i x y` est une instruction de machine à registres valide, indicée `PC` dans un programme à `NI` instructions.
"""
function chkinstruction(PC::Int64, NI::Int64, i::Int64, x::Int64, y::Int64)::Bool
	if 1 > i || i > 5
		printerr(PC, "invalid command ", i)
		return false
	end

	ok = true

	if !chkregister(PC, x)
		ok = false
	end

	if i ≡ 5 && !chklinenumber(PC, NI, y) || 1 < i < 5 && !chkregister(PC, y)
		ok = false
	end

	return ok
end

"""
	analyzesyntax(pgm)

Analyse la syntaxe du programme `pgm`, en affichant les éventuelles erreurs détectées.
Renvoie `true` si aucune erreur n'a été détectée et `false` sinon.
"""
function analyzesyntax(pgm::Matrix{Int64})::Bool
	NI = size(pgm, 1)
	ok = true

	@simd for PC in axes(pgm, 1)
		@inbounds @views i, x, y = pgm[PC,:]

		if !chkinstruction(PC, NI, i, x, y)
			ok = false
		end
	end

	return ok
end

"""
	analyzeinit(pgm)

Analyse l'initialisation des registres du programme `pgm`, en affichant les éventuelles erreurs détectées.
Renvoie toujours `true`, les erreurs d'initialisation de registres pouvant être intentionnelles.
"""
function analyzeinit(pgm::Matrix{Int64})::Bool
	context = falses(NR, size(pgm, 1))
	jumpqueue = Vector{AnalysisExecution}()
	pushfirst!(jumpqueue, AnalysisExecution(1, trues(NR), nothing))
	while !isempty(jumpqueue)
		exec = pop!(jumpqueue)
		@inbounds @views while exec.PC ≤ size(pgm, 1)
			newContext = context[:, exec.PC] .| exec.registers
			if newContext == context[:, exec.PC]
				break
			end
			context[:, exec.PC] = newContext

			instruction, x, y = pgm[exec.PC, :]

			if 3 ≤ instruction ≤ 5 && exec.registers[x]
				printwarn(exec.PC, "register ", x, " may be unset")
				printstack(exec.jumpstack)
				exec.registers[x] = false
			end
			if 2 ≤ instruction ≤ 4 && exec.registers[y]
				printwarn(exec.PC, "register ", y, " may be unset")
				printstack(exec.jumpstack)
				exec.registers[y] = false
			end

			if instruction ≡ 5
				exec2 = AnalysisExecution(y, copy(exec.registers), StackNode(exec.PC, exec.jumpstack))
				jumpqueue = pushfirst!(jumpqueue, exec2)
			else
				exec.registers[x] = false
			end

			exec.PC += 1
		end
	end

	return true
end

"""
	analyze(pgm)

Analyse le programme `pgm`, en affichant les éventuelles erreurs détectées.
Renvoie `true` si aucune erreur n'a été détectée et `false` sinon.
"""
function analyze(pgm::Matrix{Int64})::Bool
	return analyzesyntax(pgm) && analyzeinit(pgm)
end

#############
# Exécution #
#############

"""
	exec(pgm)

Exécute le programme `pgm` pour une machine à `NR` registres et affiche l'état des registres à chaque étape.
"""
function exec(pgm::Array{Int64,2})
	memR = zeros(Int64, NR)
	PC = 1
	pgmsize = size(pgm,1)
	printstate(memR, PC)
	
	while PC<=pgmsize
		ins = pgm[PC,1]
		if(ins==1)
			memR[pgm[PC,2]] = pgm[PC,3]
		elseif(ins==2)
			memR[pgm[PC,2]] = memR[pgm[PC,3]]
		elseif(ins==3)
			memR[pgm[PC,2]] = memR[pgm[PC,2]]+memR[pgm[PC,3]]
		elseif(ins==4)
			memR[pgm[PC,2]] = memR[pgm[PC,2]]-memR[pgm[PC,3]]
		end
		
		if(ins==5 && memR[pgm[PC,2]]==0)
			PC = pgm[PC,3]
		else
			PC += 1
		end
		printstate(memR, PC)
	end
end

"""
	printstate(memr, pc)

Affiche l'état des registres `memR` et la valeur de `pc`
"""
function printstate(memr, pc)
	print(memr)
	println("  PC=", pc)
end

"""
	execfile(file)

Exécute le programme à partir du fichier `file`
"""
function execfile(file::String)
	exec(filetomatrix(file))
end

########
# TOUT #
########

"""
	DAE(file)

Décompile, Analyse et Exécute le programme contenu dans le fichier `file`
"""
function DAE(file::String)
	pgmmatrix = filetomatrix(file)
	arraytofile(IHM(pgmmatrix))#décompilation
	
	if analyze(pgmmatrix)
		println("\033[1;32m-----No errors found-----\033[0m")
		exec(pgmmatrix)
	end
end

